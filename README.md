# maven

 ```
  allprojects {
  	repositories {
		...
    maven { url "https://code.aliyun.com/louisgeek/maven/raw/master" }
    maven { url "https://codeberg.org/louisgeek/maven/raw/branch/master" } 
    maven { url "https://code.gitlink.org.cn/louisgeek/maven/raw/branch/master" }
    maven { url "https://git.trustie.net/louisgeek/maven/raw/branch/master" } 
  	}
  }
  ```
  
  ```
  dependencies {
		implementation 'com.github.louisgeek:xwalk_core_library:23.53.589.4'
        implementation 'com.github.louisgeek:kaldi-android:5.2_20200502'
             
  }
  ```
